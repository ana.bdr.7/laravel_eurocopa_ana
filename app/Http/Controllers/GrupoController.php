<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Grupo;
use Illuminate\Support\Facades\DB;

class GrupoController extends Controller
{
    public function inicio(){
        return redirect()->action([GrupoController::class,'index']);
    }

    public function index(){

        $grupos = Grupo::all();

        $partidos = DB::table('partidos')
            ->where('disputado','=','0')
            ->get();

        return view('grupos.index',['grupos' => $grupos,'partidos'=>$partidos]);
    }
}
