<?php

namespace App\Http\Controllers;
use App\Models\Pais;

use Illuminate\Http\Request;

class PaisController extends Controller
{
    public function show(Pais $pais){
        $puntos = 0;
        return view('paises.show',['pais'=>$pais,'puntos' => $puntos]);
    }
}
