<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Partido;
use Illuminate\Support\Facades\DB;

class PartidoController extends Controller
{
    public function disputa($id_partido){
        
        $partido  = Partido::find($id_partido);
       
        $partido->goles_pais1 = rand(0,4);
        $partido->goles_pais2 = rand(0,3);
        $partido->disputado = 1;
        $pais = DB::table('pais')
            ->where('id','=',$partido->goles_pais1)            
            ->get();
        
        /*foreach($partido->paises() as $pais_buscar){
            if($pais_buscar->id == $partido->pais1_id){
                
                $pais_elegido = $pais_buscar;
            }
            
        }*/
        
            

        try{
            $partido->save();
            /*return redirect()->route('paises.show',$pais_elegido)->with('El partido se ha disputado');*/
            return redirect()->route('inicio')->with('El partido se ha disputado');
        }catch(Illuminate\Database\QueryException $ex){
            return redirect()->route('paises.show',$pais_elegido)->with('Ha ocurrido un error');
        }

    }
}
