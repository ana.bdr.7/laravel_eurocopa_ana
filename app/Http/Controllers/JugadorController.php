<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class JugadorController extends Controller
{
    public function busquedaAjax(Request $request){
      
        $jugador = DB::table('jugadors')
            ->where('numeroCamiseta','like','%'.$request->camiseta.'%')
            
            ->get();
        return response()->json($jugador);
    }
}
