<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Pais;
use Carbon\Carbon;


class Jugador extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function pais(){
        return $this->belongsTo(Pais::class);
    }
    public function getEdad(){
        $fechaFormateada = Carbon::parse($this->fechaNacimiento);
        return $fechaFormateada->diffInYears(Carbon::now());
    }
}
