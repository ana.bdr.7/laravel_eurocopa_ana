<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Partido extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function paises(){
        return $this->belongsTo(Pais::class,'pais1_id')->get()
            ->merge($this->belongsTo(Pais::class,'pais2_id')->get());
    }
}
