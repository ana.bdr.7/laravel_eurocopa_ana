<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Grupo;
use App\Models\Jugador;
use App\Models\Partido;
use App\Models\Pais;

class Pais extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function grupo(){
        return $this->belongsTo(Grupo::class);
    }

    public function jugadores(){
        return $this->hasMany(Jugador::class);
    }

    public function partidos(){
        return $this->hasMany(Partido::class,'pais1_id')->orWhere('pais2_id',$this->id);
    }

    public function getRouteKeyName(){
        return 'slug';
    }

}
