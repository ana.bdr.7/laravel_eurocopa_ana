@extends('layouts.master')

@section('titulo')
    Grupos
@endsection

@section('contenido')
@if(session('mensaje'))
<div class="alert alert-success">
    {{ session('mensaje') }}
</div>
@endif
<div class="row" id="container"> 


@foreach( $grupos as $clave => $grupo )
        
       <ul>Grupo {{$grupo->letra}}
       @foreach($grupo->paises as $pais)
        <li><a href="{{ route('paises.show', $pais ) }}">{{$pais->nombre}}</a>
        @foreach($pais->partidos as $partido)
            
            @if($partido->disputado == 1)
            {{$partido->disputado}}
               
               
            @endif
            
        @endforeach

        </li>
       @endforeach
       </ul>
    @endforeach
</div> 
@endsection