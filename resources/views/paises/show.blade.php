@extends('layouts.master')

@section('titulo')
    Paises
@endsection

@section('contenido')
@if(session('mensaje'))
<div class="alert alert-success">
    {{ session('mensaje') }}
</div>
@endif
<div class="row" id="container"> 

<p><h1>{{$pais->nombre}}</h1></p>

@foreach($pais->partidos as $partido)
@if($partido->disputado == 1)
    @if($partido->goles_pais1 > $partido->goles_pais2)
    @foreach($partido->paises() as $pais)
    <table>{{$pais->nombre}} -</table>
        @endforeach
        @endif
    @endif

    
   
@endforeach
<table><tr>
@foreach($pais->partidos as $partido)
@if($partido->disputado == 0)
@foreach($partido->paises() as $pais)
    <table>{{$pais->nombre}} -
        @endforeach
        (NO DISPUTADO)
        <a href="{{ route('partido.disputa', $partido->id ) }}">Jugar Partido</a></p>
        </table>
@endif
@endforeach
<tr></table>
<table><tr><th>Numero</th><th>Nombre</th><th>Posicion</th><th>Edad</th></tr>
@foreach($pais->jugadores as $jugador)
<tr><td>{{$jugador->numeroCamiseta}}</td><td>{{$jugador->nombre}}</td><td>{{$jugador->posicion}}</td><td>{{$jugador->getEdad()}}</td></tr>
@endforeach
</table>
</div> 

@endsection