<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GrupoController;
use App\Http\Controllers\PaisController;
use App\Http\Controllers\PartidoController;
use App\Http\Controllers\JugadorController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[GrupoController::class,'inicio'])->name('inicio');

Route::get('grupos',[GrupoController::class,'index'])->name('grupos.index');

Route::post('/busquedaAjax',[JugadorController::class,'busquedaAjax'])->name('jugadores.ajax');



Route::get('paises/{pais}',[PaisController::class,'show'])->name('paises.show');

Route::get('partido/{partido}/disputar',[PartidoController::class,'disputa'])->name('partido.disputa');
