<?php

namespace Database\Factories;


use App\Models\Jugador;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class JugadorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Jugador::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->name('male'),
            'numeroCamiseta' => rand(1,25),
            'fechaNacimiento' => $this->faker->date,
            'posicion' => $this->faker->name,
            'pais_id' => random_int(\DB::table('pais')->min('id'), \DB::table('pais')->max('id'))
        ];
    }
}
