<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partidos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pais1_id')->unsigned();
            $table->foreign('pais1_id')->references('id')->on('pais');
            $table->integer('pais2_id')->unsigned();
            $table->foreign('pais2_id')->references('id')->on('pais');
            $table->integer('goles_pais1');
            $table->integer('goles_pais2');
            $table->integer('disputado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partidos');
    }
}
